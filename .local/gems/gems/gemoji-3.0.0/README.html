<h1 id="gemoji">gemoji</h1>
<p>This library contains character information about native emoji, as well as image files for a few custom emoji.</p>
<h2 id="installation">Installation</h2>
<p>Add <code>gemoji</code> to your Gemfile.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb1-1" title="1">gem <span class="st">&#39;gemoji&#39;</span></a></code></pre></div>
<h3 id="extract-images">Extract images</h3>
<p>To obtain image files as fallbacks for browsers and OS’s that don’t support emoji, run the <code>gemoji extract</code> command <strong>on macOS Sierra or later</strong>:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><a class="sourceLine" id="cb2-1" title="1"><span class="ex">bundle</span> exec gemoji extract public/images/emoji</a></code></pre></div>
<p>This will extract images into filenames such as:</p>
<ul>
<li><code>public/images/emoji/octocat.png</code></li>
<li><code>public/images/emoji/unicode/1f9c0.png</code> (the <code>:cheese:</code> emoji)</li>
</ul>
<h2 id="example-rails-helper">Example Rails Helper</h2>
<p>This would allow emojifying content such as: <code>it's raining :cat:s and :dog:s!</code></p>
<p>See the <a href="http://www.emoji-cheat-sheet.com">Emoji cheat sheet</a> for more examples.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb3-1" title="1"><span class="kw">module</span> <span class="dt">EmojiHelper</span></a>
<a class="sourceLine" id="cb3-2" title="2">  <span class="kw">def</span> emojify(content)</a>
<a class="sourceLine" id="cb3-3" title="3">    h(content).to_str.gsub(<span class="ot">/:([\w+-]+):/</span>) <span class="kw">do</span> |match|</a>
<a class="sourceLine" id="cb3-4" title="4">      <span class="kw">if</span> emoji = <span class="dt">Emoji</span>.find_by_alias(<span class="dt">$1</span>)</a>
<a class="sourceLine" id="cb3-5" title="5"><span class="ot">        %(</span><span class="st">&lt;img alt=&quot;#$1&quot; src=&quot;</span><span class="ot">#{</span>image_path(<span class="st">&quot;emoji/</span><span class="ot">#{</span>emoji.image_filename<span class="ot">}</span><span class="st">&quot;</span>)<span class="ot">}</span><span class="st">&quot; style=&quot;vertical-align:middle&quot; width=&quot;20&quot; height=&quot;20&quot; /&gt;</span><span class="ot">)</span></a>
<a class="sourceLine" id="cb3-6" title="6">      <span class="kw">else</span></a>
<a class="sourceLine" id="cb3-7" title="7">        match</a>
<a class="sourceLine" id="cb3-8" title="8">      <span class="kw">end</span></a>
<a class="sourceLine" id="cb3-9" title="9">    <span class="kw">end</span>.html_safe <span class="kw">if</span> content.present?</a>
<a class="sourceLine" id="cb3-10" title="10">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb3-11" title="11"><span class="kw">end</span></a></code></pre></div>
<h2 id="unicode-mapping">Unicode mapping</h2>
<p>Translate emoji names to unicode and vice versa.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb4-1" title="1">&gt;&gt; <span class="dt">Emoji</span>.find_by_alias(<span class="st">&quot;cat&quot;</span>).raw</a>
<a class="sourceLine" id="cb4-2" title="2">=&gt; <span class="st">&quot;🐱&quot;</span>  <span class="co"># Don&#39;t see a cat? That&#39;s U+1F431.</span></a>
<a class="sourceLine" id="cb4-3" title="3"></a>
<a class="sourceLine" id="cb4-4" title="4">&gt;&gt; <span class="dt">Emoji</span>.find_by_unicode(<span class="st">&quot;\u{1f431}&quot;</span>).name</a>
<a class="sourceLine" id="cb4-5" title="5">=&gt; <span class="st">&quot;cat&quot;</span></a></code></pre></div>
<h2 id="adding-new-emoji">Adding new emoji</h2>
<p>You can add new emoji characters to the <code>Emoji.all</code> list:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb5-1" title="1">emoji = <span class="dt">Emoji</span>.create(<span class="st">&quot;music&quot;</span>) <span class="kw">do</span> |char|</a>
<a class="sourceLine" id="cb5-2" title="2">  char.add_alias <span class="st">&quot;song&quot;</span></a>
<a class="sourceLine" id="cb5-3" title="3">  char.add_unicode_alias <span class="st">&quot;\u{266b}&quot;</span></a>
<a class="sourceLine" id="cb5-4" title="4">  char.add_tag <span class="st">&quot;notes&quot;</span></a>
<a class="sourceLine" id="cb5-5" title="5"><span class="kw">end</span></a>
<a class="sourceLine" id="cb5-6" title="6"></a>
<a class="sourceLine" id="cb5-7" title="7">emoji.name <span class="co">#=&gt; &quot;music&quot;</span></a>
<a class="sourceLine" id="cb5-8" title="8">emoji.raw  <span class="co">#=&gt; &quot;♫&quot;</span></a>
<a class="sourceLine" id="cb5-9" title="9">emoji.image_filename <span class="co">#=&gt; &quot;unicode/266b.png&quot;</span></a>
<a class="sourceLine" id="cb5-10" title="10"></a>
<a class="sourceLine" id="cb5-11" title="11"><span class="co"># Creating custom emoji (no Unicode aliases):</span></a>
<a class="sourceLine" id="cb5-12" title="12">emoji = <span class="dt">Emoji</span>.create(<span class="st">&quot;music&quot;</span>) <span class="kw">do</span> |char|</a>
<a class="sourceLine" id="cb5-13" title="13">  char.add_tag <span class="st">&quot;notes&quot;</span></a>
<a class="sourceLine" id="cb5-14" title="14"><span class="kw">end</span></a>
<a class="sourceLine" id="cb5-15" title="15"></a>
<a class="sourceLine" id="cb5-16" title="16">emoji.custom? <span class="co">#=&gt; true</span></a>
<a class="sourceLine" id="cb5-17" title="17">emoji.image_filename <span class="co">#=&gt; &quot;music.png&quot;</span></a></code></pre></div>
<p>As you create new emoji, you must ensure that you also create and put the images they reference by their <code>image_filename</code> to your assets directory.</p>
<p>You can customize <code>image_filename</code> with:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb6-1" title="1">emoji = <span class="dt">Emoji</span>.create(<span class="st">&quot;music&quot;</span>) <span class="kw">do</span> |char|</a>
<a class="sourceLine" id="cb6-2" title="2">  char.image_filename = <span class="st">&quot;subdirectory/my_emoji.gif&quot;</span></a>
<a class="sourceLine" id="cb6-3" title="3"><span class="kw">end</span></a></code></pre></div>
<p>For existing emojis, you can edit the list of aliases or add new tags in an edit block:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb7-1" title="1">emoji = <span class="dt">Emoji</span>.find_by_alias <span class="st">&quot;musical_note&quot;</span></a>
<a class="sourceLine" id="cb7-2" title="2"></a>
<a class="sourceLine" id="cb7-3" title="3"><span class="dt">Emoji</span>.edit_emoji(emoji) <span class="kw">do</span> |char|</a>
<a class="sourceLine" id="cb7-4" title="4">  char.add_alias <span class="st">&quot;music&quot;</span></a>
<a class="sourceLine" id="cb7-5" title="5">  char.add_unicode_alias <span class="st">&quot;\u{266b}&quot;</span></a>
<a class="sourceLine" id="cb7-6" title="6">  char.add_tag <span class="st">&quot;notes&quot;</span></a>
<a class="sourceLine" id="cb7-7" title="7"><span class="kw">end</span></a>
<a class="sourceLine" id="cb7-8" title="8"></a>
<a class="sourceLine" id="cb7-9" title="9"><span class="dt">Emoji</span>.find_by_alias <span class="st">&quot;music&quot;</span>       <span class="co">#=&gt; emoji</span></a>
<a class="sourceLine" id="cb7-10" title="10"><span class="dt">Emoji</span>.find_by_unicode <span class="st">&quot;\u{266b}&quot;</span>  <span class="co">#=&gt; emoji</span></a></code></pre></div>
