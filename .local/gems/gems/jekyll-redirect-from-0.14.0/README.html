<h1 id="jekyllredirectfrom">JekyllRedirectFrom</h1>
<p>Give your Jekyll posts and pages multiple URLs.</p>
<p>When importing your posts and pages from, say, Tumblr, it’s annoying and impractical to create new pages in the proper subdirectories so they, e.g. <code>/post/123456789/my-slug-that-is-often-incompl</code>, redirect to the new post URL.</p>
<p>Instead of dealing with maintaining those pages for redirection, let <code>jekyll-redirect-from</code> handle it for you.</p>
<p><a href="https://travis-ci.org/jekyll/jekyll-redirect-from"><img src="https://travis-ci.org/jekyll/jekyll-redirect-from.svg?branch=master" alt="Build Status" /></a></p>
<h2 id="how-it-works">How it Works</h2>
<p>Redirects are performed by serving an HTML file with an HTTP-REFRESH meta tag which points to your destination. No <code>.htaccess</code> file, nginx conf, xml file, or anything else will be generated. It simply creates HTML files.</p>
<h2 id="installation">Installation</h2>
<p>Add this line to your application’s Gemfile:</p>
<pre><code>gem &#39;jekyll-redirect-from&#39;</code></pre>
<p>And then execute:</p>
<pre><code>$ bundle</code></pre>
<p>Or install it yourself as:</p>
<pre><code>$ gem install jekyll-redirect-from</code></pre>
<p>Once it’s installed into your evironment, add it to your <code>_config.yml</code>:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb4-1" title="1"><span class="fu">plugins:</span></a>
<a class="sourceLine" id="cb4-2" title="2">  <span class="kw">-</span> jekyll-redirect-from</a></code></pre></div>
<p>💡 If you are using a Jekyll version less than 3.5.0, use the <code>gems</code> key instead of <code>plugins</code>.</p>
<p>If you’re using Jekyll in <code>safe</code> mode to mimic GitHub Pages, make sure to add jekyll-redirect-from to your whitelist:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb5-1" title="1"><span class="fu">whitelist:</span></a>
<a class="sourceLine" id="cb5-2" title="2">  <span class="kw">-</span> jekyll-redirect-from</a></code></pre></div>
<p>Then run <code>jekyll &lt;cmd&gt; --safe</code> like normal.</p>
<h2 id="usage">Usage</h2>
<p>The object of this gem is to allow an author to specify multiple URLs for a page, such that the alternative URLs redirect to the new Jekyll URL.</p>
<p>To use it, simply add the array to the YAML front-matter of your page or post:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb6-1" title="1"><span class="fu">title:</span><span class="at"> My amazing post</span></a>
<a class="sourceLine" id="cb6-2" title="2"><span class="fu">redirect_from:</span></a>
<a class="sourceLine" id="cb6-3" title="3">  <span class="kw">-</span> /post/123456789/</a>
<a class="sourceLine" id="cb6-4" title="4">  <span class="kw">-</span> /post/123456789/my-amazing-post/</a></code></pre></div>
<p>Redirects including a trailing slash will generate a corresponding subdirectory containing an <code>index.html</code>, while redirects without a trailing slash will generate a corresponding <code>filename</code> without an extension, and without a subdirectory.</p>
<p>For example…</p>
<pre class="text"><code>redirect_from:
  - /post/123456789/my-amazing-post</code></pre>
<p>…will generate the following page in the destination:</p>
<pre class="text"><code>/post/123456789/my-amazing-post</code></pre>
<p>While…</p>
<pre class="text"><code>redirect_from:
  - /post/123456789/my-amazing-post/</code></pre>
<p>…will generate the following page in the destination:</p>
<pre class="text"><code>/post/123456789/my-amazing-post/index.html</code></pre>
<p>These pages will contain an HTTP-REFRESH meta tag which redirect to your URL.</p>
<p>You can also specify just <strong>one url</strong> like this:</p>
<pre class="text"><code>title: My other awesome post
redirect_from: /post/123456798/</code></pre>
<h3 id="prefix">Prefix</h3>
<p>If <code>site.url</code> is set, its value, together with <code>site.baseurl</code>, is used as a prefix for the redirect url automatically. This is useful for scenarios where a site isn’t available from the domain root, so the redirects point to the correct path. If <code>site.url</code> is not set, only <code>site.baseurl</code> is used, if set.</p>
<p><strong><em>Note</em></strong>: If you are hosting your Jekyll site on <a href="https://pages.github.com/">GitHub Pages</a>, and <code>site.url</code> is not set, the prefix is set to the pages domain name i.e. http://example.github.io/project or a custom CNAME.</p>
<h3 id="redirect-to">Redirect To</h3>
<p>Sometimes, you may want to redirect a site page to a totally different website. This plugin also supports that with the <code>redirect_to</code> key:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb12-1" title="1"><span class="fu">title:</span><span class="at"> My amazing post</span></a>
<a class="sourceLine" id="cb12-2" title="2"><span class="fu">redirect_to:</span></a>
<a class="sourceLine" id="cb12-3" title="3">  <span class="kw">-</span> http://www.github.com</a></code></pre></div>
<p>If you have multiple <code>redirect_to</code>s set, only the first one will be respected.</p>
<p><strong>Note</strong>: Using <code>redirect_to</code> or <code>redirect_from</code> with collections will only work with files which are output to HTML, such as <code>.md</code>, <code>.textile</code>, <code>.html</code> etc.</p>
<h2 id="customizing-the-redirect-template">Customizing the redirect template</h2>
<p>If you want to customize the redirect template, you can. Simply create a layout in your site’s <code>_layouts</code> directory called <code>redirect.html</code>.</p>
<p>Your layout will get the following variables:</p>
<ul>
<li><code>page.redirect.from</code> - the relative path to the redirect page</li>
<li><code>page.redirect.to</code> - the absolute URL (where available) to the target page</li>
</ul>
<h2 id="contributing">Contributing</h2>
<ol type="1">
<li>Fork it</li>
<li>Create your feature branch (<code>git checkout -b my-new-feature</code>)</li>
<li>Commit your changes (<code>git commit -am 'Add some feature'</code>)</li>
<li>Push to the branch (<code>git push origin my-new-feature</code>)</li>
<li>Create new Pull Request</li>
</ol>
