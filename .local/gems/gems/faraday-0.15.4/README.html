<h1 id="faraday">Faraday</h1>
<p><a href="https://rubygems.org/gems/faraday"><img src="https://badge.fury.io/rb/faraday.svg" alt="Gem Version" /></a> <a href="https://travis-ci.org/lostisland/faraday"><img src="https://travis-ci.org/lostisland/faraday.svg" alt="Build Status" /></a> <a href="https://coveralls.io/github/lostisland/faraday?branch=master"><img src="https://coveralls.io/repos/github/lostisland/faraday/badge.svg?branch=master" alt="Coverage Status" /></a> <a href="https://codeclimate.com/github/lostisland/faraday"><img src="https://codeclimate.com/github/lostisland/faraday/badges/gpa.svg" alt="Code Climate" /></a> <a href="https://gitter.im/lostisland/faraday?utm_source=badge&amp;utm_medium=badge&amp;utm_campaign=pr-badge"><img src="https://badges.gitter.im/lostisland/faraday.svg" alt="Gitter" /></a></p>
<p>Faraday is an HTTP client lib that provides a common interface over many adapters (such as Net::HTTP) and embraces the concept of Rack middleware when processing the request/response cycle.</p>
<p>Faraday supports these adapters out of the box:</p>
<ul>
<li><a href="http://ruby-doc.org/stdlib/libdoc/net/http/rdoc/Net/HTTP.html">Net::HTTP</a> <em>(default)</em></li>
<li><a href="https://github.com/drbrain/net-http-persistent">Net::HTTP::Persistent</a></li>
<li><a href="https://github.com/excon/excon#readme">Excon</a></li>
<li><a href="http://toland.github.io/patron/">Patron</a></li>
<li><a href="https://github.com/igrigorik/em-http-request#readme">EventMachine</a></li>
<li><a href="https://github.com/nahi/httpclient">HTTPClient</a></li>
</ul>
<p>Adapters are slowly being moved into their own gems, or bundled with HTTP clients:</p>
<ul>
<li><a href="https://github.com/typhoeus/typhoeus/blob/master/lib/typhoeus/adapters/faraday.rb">Typhoeus</a></li>
</ul>
<p>It also includes a Rack adapter for hitting loaded Rack applications through Rack::Test, and a Test adapter for stubbing requests by hand.</p>
<h2 id="api-documentation">API documentation</h2>
<p>Available at <a href="http://www.rubydoc.info/gems/faraday">rubydoc.info</a>.</p>
<h2 id="usage">Usage</h2>
<h3 id="basic-use">Basic Use</h3>
<div class="sourceCode" id="cb1"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb1-1" title="1">response = <span class="dt">Faraday</span>.get <span class="st">&#39;http://sushi.com/nigiri/sake.json&#39;</span></a></code></pre></div>
<p>A simple <code>get</code> request can be performed by using the syntax described above. This works if you don’t need to set up anything; you can roll with just the default middleware stack and default adapter (see <a href="https://github.com/lostisland/faraday/blob/master/lib/faraday/rack_builder.rb">Faraday::RackBuilder#initialize</a>).</p>
<p>A more flexible way to use Faraday is to start with a Connection object. If you want to keep the same defaults, you can use this syntax:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb2-1" title="1">conn = <span class="dt">Faraday</span>.new(<span class="st">:url</span> =&gt; <span class="st">&#39;http://www.example.com&#39;</span>)</a>
<a class="sourceLine" id="cb2-2" title="2">response = conn.get <span class="st">&#39;/users&#39;</span>                 <span class="co"># GET http://www.example.com/users&#39;</span></a></code></pre></div>
<p>Connections can also take an options hash as a parameter or be configured by using a block. Checkout the section called <a href="#advanced-middleware-usage">Advanced middleware usage</a> for more details about how to use this block for configurations. Since the default middleware stack uses url_encoded middleware and default adapter, use them on building your own middleware stack.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb3-1" title="1">conn = <span class="dt">Faraday</span>.new(<span class="st">:url</span> =&gt; <span class="st">&#39;http://sushi.com&#39;</span>) <span class="kw">do</span> |faraday|</a>
<a class="sourceLine" id="cb3-2" title="2">  faraday.request  <span class="st">:url_encoded</span>             <span class="co"># form-encode POST params</span></a>
<a class="sourceLine" id="cb3-3" title="3">  faraday.response <span class="st">:logger</span>                  <span class="co"># log requests to $stdout</span></a>
<a class="sourceLine" id="cb3-4" title="4">  faraday.adapter  <span class="dt">Faraday</span>.default_adapter  <span class="co"># make requests with Net::HTTP</span></a>
<a class="sourceLine" id="cb3-5" title="5"><span class="kw">end</span></a>
<a class="sourceLine" id="cb3-6" title="6"></a>
<a class="sourceLine" id="cb3-7" title="7"><span class="co"># Filter sensitive information from logs with a regex matcher</span></a>
<a class="sourceLine" id="cb3-8" title="8"></a>
<a class="sourceLine" id="cb3-9" title="9">conn = <span class="dt">Faraday</span>.new(<span class="st">:url</span> =&gt; <span class="st">&#39;http://sushi.com/api_key=s3cr3t&#39;</span>) <span class="kw">do</span> |faraday|</a>
<a class="sourceLine" id="cb3-10" title="10">  faraday.request  <span class="st">:url_encoded</span>             <span class="co"># form-encode POST params</span></a>
<a class="sourceLine" id="cb3-11" title="11">  faraday.response <span class="st">:logger</span> <span class="kw">do</span> | logger |</a>
<a class="sourceLine" id="cb3-12" title="12">    logger.filter(<span class="ot">/(api_key=)(\w+)/</span>,<span class="st">&#39;\1[REMOVED]&#39;</span>)</a>
<a class="sourceLine" id="cb3-13" title="13">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb3-14" title="14">  faraday.adapter  <span class="dt">Faraday</span>.default_adapter  <span class="co"># make requests with Net::HTTP</span></a>
<a class="sourceLine" id="cb3-15" title="15"><span class="kw">end</span></a></code></pre></div>
<p>Once you have the connection object, use it to make HTTP requests. You can pass parameters to it in a few different ways:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb4-1" title="1"><span class="co">## GET ##</span></a>
<a class="sourceLine" id="cb4-2" title="2"></a>
<a class="sourceLine" id="cb4-3" title="3">response = conn.get <span class="st">&#39;/nigiri/sake.json&#39;</span>     <span class="co"># GET http://sushi.com/nigiri/sake.json</span></a>
<a class="sourceLine" id="cb4-4" title="4">response.body</a>
<a class="sourceLine" id="cb4-5" title="5"></a>
<a class="sourceLine" id="cb4-6" title="6">conn.get <span class="st">&#39;/nigiri&#39;</span>, { <span class="st">:name</span> =&gt; <span class="st">&#39;Maguro&#39;</span> }   <span class="co"># GET http://sushi.com/nigiri?name=Maguro</span></a>
<a class="sourceLine" id="cb4-7" title="7"></a>
<a class="sourceLine" id="cb4-8" title="8">conn.get <span class="kw">do</span> |req|                           <span class="co"># GET http://sushi.com/search?page=2&amp;limit=100</span></a>
<a class="sourceLine" id="cb4-9" title="9">  req.url <span class="st">&#39;/search&#39;</span>, <span class="st">:page</span> =&gt; <span class="dv">2</span></a>
<a class="sourceLine" id="cb4-10" title="10">  req.params[<span class="st">&#39;limit&#39;</span>] = <span class="dv">100</span></a>
<a class="sourceLine" id="cb4-11" title="11"><span class="kw">end</span></a>
<a class="sourceLine" id="cb4-12" title="12"></a>
<a class="sourceLine" id="cb4-13" title="13"><span class="co">## POST ##</span></a>
<a class="sourceLine" id="cb4-14" title="14"></a>
<a class="sourceLine" id="cb4-15" title="15">conn.post <span class="st">&#39;/nigiri&#39;</span>, { <span class="st">:name</span> =&gt; <span class="st">&#39;Maguro&#39;</span> }  <span class="co"># POST &quot;name=maguro&quot; to http://sushi.com/nigiri</span></a></code></pre></div>
<p>Some configuration options can be adjusted per request:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb5-1" title="1"><span class="co"># post payload as JSON instead of &quot;www-form-urlencoded&quot; encoding:</span></a>
<a class="sourceLine" id="cb5-2" title="2">conn.post <span class="kw">do</span> |req|</a>
<a class="sourceLine" id="cb5-3" title="3">  req.url <span class="st">&#39;/nigiri&#39;</span></a>
<a class="sourceLine" id="cb5-4" title="4">  req.headers[<span class="st">&#39;Content-Type&#39;</span>] = <span class="st">&#39;application/json&#39;</span></a>
<a class="sourceLine" id="cb5-5" title="5">  req.body = <span class="st">&#39;{ &quot;name&quot;: &quot;Unagi&quot; }&#39;</span></a>
<a class="sourceLine" id="cb5-6" title="6"><span class="kw">end</span></a>
<a class="sourceLine" id="cb5-7" title="7"></a>
<a class="sourceLine" id="cb5-8" title="8"><span class="co">## Per-request options ##</span></a>
<a class="sourceLine" id="cb5-9" title="9"></a>
<a class="sourceLine" id="cb5-10" title="10">conn.get <span class="kw">do</span> |req|</a>
<a class="sourceLine" id="cb5-11" title="11">  req.url <span class="st">&#39;/search&#39;</span></a>
<a class="sourceLine" id="cb5-12" title="12">  req.options.timeout = <span class="dv">5</span>           <span class="co"># open/read timeout in seconds</span></a>
<a class="sourceLine" id="cb5-13" title="13">  req.options.open_timeout = <span class="dv">2</span>      <span class="co"># connection open timeout in seconds</span></a>
<a class="sourceLine" id="cb5-14" title="14"><span class="kw">end</span></a></code></pre></div>
<p>And you can inject arbitrary data into the request using the <code>context</code> option:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb6-1" title="1"><span class="co"># Anything you inject using context option will be available in the env on all middlewares</span></a>
<a class="sourceLine" id="cb6-2" title="2"></a>
<a class="sourceLine" id="cb6-3" title="3">conn.get <span class="kw">do</span> |req|</a>
<a class="sourceLine" id="cb6-4" title="4">  req.url <span class="st">&#39;/search&#39;</span></a>
<a class="sourceLine" id="cb6-5" title="5">  req.options.context = {</a>
<a class="sourceLine" id="cb6-6" title="6">      <span class="st">foo: &#39;foo&#39;</span>,</a>
<a class="sourceLine" id="cb6-7" title="7">      <span class="st">bar: &#39;bar&#39;</span></a>
<a class="sourceLine" id="cb6-8" title="8">  }</a>
<a class="sourceLine" id="cb6-9" title="9"><span class="kw">end</span></a></code></pre></div>
<h3 id="changing-how-parameters-are-serialized">Changing how parameters are serialized</h3>
<p>Sometimes you need to send the same URL parameter multiple times with different values. This requires manually setting the parameter encoder and can be done on either per-connection or per-request basis.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb7-1" title="1"><span class="co"># per-connection setting</span></a>
<a class="sourceLine" id="cb7-2" title="2">conn = <span class="dt">Faraday</span>.new <span class="st">:request</span> =&gt; { <span class="st">:params_encoder</span> =&gt; <span class="dt">Faraday</span>::<span class="dt">FlatParamsEncoder</span> }</a>
<a class="sourceLine" id="cb7-3" title="3"></a>
<a class="sourceLine" id="cb7-4" title="4">conn.get <span class="kw">do</span> |req|</a>
<a class="sourceLine" id="cb7-5" title="5">  <span class="co"># per-request setting:</span></a>
<a class="sourceLine" id="cb7-6" title="6">  <span class="co"># req.options.params_encoder = my_encoder</span></a>
<a class="sourceLine" id="cb7-7" title="7">  req.params[<span class="st">&#39;roll&#39;</span>] = [<span class="st">&#39;california&#39;</span>, <span class="st">&#39;philadelphia&#39;</span>]</a>
<a class="sourceLine" id="cb7-8" title="8"><span class="kw">end</span></a>
<a class="sourceLine" id="cb7-9" title="9"><span class="co"># GET &#39;http://sushi.com?roll=california&amp;roll=philadelphia&#39;</span></a></code></pre></div>
<p>The value of Faraday <code>params_encoder</code> can be any object that responds to:</p>
<ul>
<li><code>encode(hash) #=&gt; String</code></li>
<li><code>decode(string) #=&gt; Hash</code></li>
</ul>
<p>The encoder will affect both how query strings are processed and how POST bodies get serialized. The default encoder is Faraday::NestedParamsEncoder.</p>
<h2 id="authentication">Authentication</h2>
<p>Basic and Token authentication are handled by Faraday::Request::BasicAuthentication and Faraday::Request::TokenAuthentication respectively. These can be added as middleware manually or through the helper methods.</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb8-1" title="1"><span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |conn|</a>
<a class="sourceLine" id="cb8-2" title="2">  conn.basic_auth(<span class="st">&#39;username&#39;</span>, <span class="st">&#39;password&#39;</span>)</a>
<a class="sourceLine" id="cb8-3" title="3"><span class="kw">end</span></a>
<a class="sourceLine" id="cb8-4" title="4"></a>
<a class="sourceLine" id="cb8-5" title="5"><span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |conn|</a>
<a class="sourceLine" id="cb8-6" title="6">  conn.token_auth(<span class="st">&#39;authentication-token&#39;</span>)</a>
<a class="sourceLine" id="cb8-7" title="7"><span class="kw">end</span></a></code></pre></div>
<h2 id="proxy">Proxy</h2>
<p>Faraday will try to automatically infer the proxy settings from your system using <code>URI#find_proxy</code>. This will retrieve them from environment variables such as http_proxy, ftp_proxy, no_proxy, etc. If for any reason you want to disable this behaviour, you can do so by setting the global varibale <code>ignore_env_proxy</code>:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb9-1" title="1"><span class="dt">Faraday</span>.ignore_env_proxy = <span class="dv">true</span></a></code></pre></div>
<p>You can also specify a custom proxy when initializing the connection</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb10-1" title="1"><span class="dt">Faraday</span>.new(<span class="st">&#39;http://www.example.com&#39;</span>, <span class="st">:proxy</span> =&gt; <span class="st">&#39;http://proxy.com&#39;</span>)</a></code></pre></div>
<h2 id="advanced-middleware-usage">Advanced middleware usage</h2>
<p>The order in which middleware is stacked is important. Like with Rack, the first middleware on the list wraps all others, while the last middleware is the innermost one, so that must be the adapter.</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb11-1" title="1"><span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |conn|</a>
<a class="sourceLine" id="cb11-2" title="2">  <span class="co"># POST/PUT params encoders:</span></a>
<a class="sourceLine" id="cb11-3" title="3">  conn.request <span class="st">:multipart</span></a>
<a class="sourceLine" id="cb11-4" title="4">  conn.request <span class="st">:url_encoded</span></a>
<a class="sourceLine" id="cb11-5" title="5"></a>
<a class="sourceLine" id="cb11-6" title="6">  <span class="co"># Last middleware must be the adapter:</span></a>
<a class="sourceLine" id="cb11-7" title="7">  conn.adapter <span class="st">:net_http</span></a>
<a class="sourceLine" id="cb11-8" title="8"><span class="kw">end</span></a></code></pre></div>
<p>This request middleware setup affects POST/PUT requests in the following way:</p>
<ol type="1">
<li><code>Request::Multipart</code> checks for files in the payload, otherwise leaves everything untouched;</li>
<li><code>Request::UrlEncoded</code> encodes as “application/x-www-form-urlencoded” if not already encoded or of another type</li>
</ol>
<p>Swapping middleware means giving the other priority. Specifying the “Content-Type” for the request is explicitly stating which middleware should process it.</p>
<p>Examples:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb12-1" title="1"><span class="co"># uploading a file:</span></a>
<a class="sourceLine" id="cb12-2" title="2">payload[<span class="st">:profile_pic</span>] = <span class="dt">Faraday</span>::<span class="dt">UploadIO</span>.new(<span class="st">&#39;/path/to/avatar.jpg&#39;</span>, <span class="st">&#39;image/jpeg&#39;</span>)</a>
<a class="sourceLine" id="cb12-3" title="3"></a>
<a class="sourceLine" id="cb12-4" title="4"><span class="co"># &quot;Multipart&quot; middleware detects files and encodes with &quot;multipart/form-data&quot;:</span></a>
<a class="sourceLine" id="cb12-5" title="5">conn.put <span class="st">&#39;/profile&#39;</span>, payload</a></code></pre></div>
<h2 id="writing-middleware">Writing middleware</h2>
<p>Middleware are classes that implement a <code>call</code> instance method. They hook into the request/response cycle.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb13-1" title="1"><span class="kw">def</span> call(request_env)</a>
<a class="sourceLine" id="cb13-2" title="2">  <span class="co"># do something with the request</span></a>
<a class="sourceLine" id="cb13-3" title="3">  <span class="co"># request_env[:request_headers].merge!(...)</span></a>
<a class="sourceLine" id="cb13-4" title="4"></a>
<a class="sourceLine" id="cb13-5" title="5">  <span class="ot">@app</span>.call(request_env).on_complete <span class="kw">do</span> |response_env|</a>
<a class="sourceLine" id="cb13-6" title="6">    <span class="co"># do something with the response</span></a>
<a class="sourceLine" id="cb13-7" title="7">    <span class="co"># response_env[:response_headers].merge!(...)</span></a>
<a class="sourceLine" id="cb13-8" title="8">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb13-9" title="9"><span class="kw">end</span></a></code></pre></div>
<p>It’s important to do all processing of the response only in the <code>on_complete</code> block. This enables middleware to work in parallel mode where requests are asynchronous.</p>
<p>The <code>env</code> is a hash with symbol keys that contains info about the request and, later, response. Some keys are:</p>
<pre><code># request phase
:method - :get, :post, ...
:url    - URI for the current request; also contains GET parameters
:body   - POST parameters for :post/:put requests
:request_headers

# response phase
:status - HTTP response status code, such as 200
:body   - the response body
:response_headers</code></pre>
<h2 id="ad-hoc-adapters-customization">Ad-hoc adapters customization</h2>
<p>Faraday is intended to be a generic interface between your code and the adapter. However, sometimes you need to access a feature specific to one of the adapters that is not covered in Faraday’s interface.</p>
<p>When that happens, you can pass a block when specifying the adapter to customize it. The block parameter will change based on the adapter you’re using. See below for some examples.</p>
<h3 id="nethttp">NetHttp</h3>
<div class="sourceCode" id="cb15"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb15-1" title="1">conn = <span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |f|</a>
<a class="sourceLine" id="cb15-2" title="2">  f.adapter <span class="st">:net_http</span> <span class="kw">do</span> |http| <span class="co"># yields Net::HTTP</span></a>
<a class="sourceLine" id="cb15-3" title="3">    http.idle_timeout = <span class="dv">100</span></a>
<a class="sourceLine" id="cb15-4" title="4">    http.verify_callback = lambda <span class="kw">do</span> | preverify_ok, cert_store |</a>
<a class="sourceLine" id="cb15-5" title="5">      <span class="co"># do something here...</span></a>
<a class="sourceLine" id="cb15-6" title="6">    <span class="kw">end</span></a>
<a class="sourceLine" id="cb15-7" title="7">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb15-8" title="8"><span class="kw">end</span></a></code></pre></div>
<h3 id="nethttppersistent">NetHttpPersistent</h3>
<div class="sourceCode" id="cb16"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb16-1" title="1">conn = <span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |f|</a>
<a class="sourceLine" id="cb16-2" title="2">  f.adapter <span class="st">:net_http_persistent</span>, <span class="st">pool_size: </span><span class="dv">5</span> <span class="kw">do</span> |http| <span class="co"># yields Net::HTTP::Persistent</span></a>
<a class="sourceLine" id="cb16-3" title="3">    http.idle_timeout = <span class="dv">100</span></a>
<a class="sourceLine" id="cb16-4" title="4">    http.retry_change_requests = <span class="dv">true</span></a>
<a class="sourceLine" id="cb16-5" title="5">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb16-6" title="6"><span class="kw">end</span></a></code></pre></div>
<h3 id="patron">Patron</h3>
<div class="sourceCode" id="cb17"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb17-1" title="1">conn = <span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |f|</a>
<a class="sourceLine" id="cb17-2" title="2">  f.adapter <span class="st">:patron</span> <span class="kw">do</span> |session| <span class="co"># yields Patron::Session</span></a>
<a class="sourceLine" id="cb17-3" title="3">    session.max_redirects = <span class="dv">10</span></a>
<a class="sourceLine" id="cb17-4" title="4">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb17-5" title="5"><span class="kw">end</span></a></code></pre></div>
<h3 id="httpclient">HTTPClient</h3>
<div class="sourceCode" id="cb18"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb18-1" title="1">conn = <span class="dt">Faraday</span>.new(...) <span class="kw">do</span> |f|</a>
<a class="sourceLine" id="cb18-2" title="2">  f.adapter <span class="st">:httpclient</span> <span class="kw">do</span> |client| <span class="co"># yields HTTPClient</span></a>
<a class="sourceLine" id="cb18-3" title="3">    client.keep_alive_timeout = <span class="dv">20</span></a>
<a class="sourceLine" id="cb18-4" title="4">    client.ssl_config.timeout = <span class="dv">25</span></a>
<a class="sourceLine" id="cb18-5" title="5">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb18-6" title="6"><span class="kw">end</span></a></code></pre></div>
<h2 id="using-faraday-for-testing">Using Faraday for testing</h2>
<div class="sourceCode" id="cb19"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb19-1" title="1"><span class="co"># It&#39;s possible to define stubbed request outside a test adapter block.</span></a>
<a class="sourceLine" id="cb19-2" title="2">stubs = <span class="dt">Faraday</span>::<span class="dt">Adapter</span>::<span class="dt">Test</span>::<span class="dt">Stubs</span>.new <span class="kw">do</span> |stub|</a>
<a class="sourceLine" id="cb19-3" title="3">  stub.get(<span class="st">&#39;/tamago&#39;</span>) { |env| [<span class="dv">200</span>, {}, <span class="st">&#39;egg&#39;</span>] }</a>
<a class="sourceLine" id="cb19-4" title="4"><span class="kw">end</span></a>
<a class="sourceLine" id="cb19-5" title="5"></a>
<a class="sourceLine" id="cb19-6" title="6"><span class="co"># You can pass stubbed request to the test adapter or define them in a block</span></a>
<a class="sourceLine" id="cb19-7" title="7"><span class="co"># or a combination of the two.</span></a>
<a class="sourceLine" id="cb19-8" title="8">test = <span class="dt">Faraday</span>.new <span class="kw">do</span> |builder|</a>
<a class="sourceLine" id="cb19-9" title="9">  builder.adapter <span class="st">:test</span>, stubs <span class="kw">do</span> |stub|</a>
<a class="sourceLine" id="cb19-10" title="10">    stub.get(<span class="st">&#39;/ebi&#39;</span>) { |env| [ <span class="dv">200</span>, {}, <span class="st">&#39;shrimp&#39;</span> ]}</a>
<a class="sourceLine" id="cb19-11" title="11">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb19-12" title="12"><span class="kw">end</span></a>
<a class="sourceLine" id="cb19-13" title="13"></a>
<a class="sourceLine" id="cb19-14" title="14"><span class="co"># It&#39;s also possible to stub additional requests after the connection has</span></a>
<a class="sourceLine" id="cb19-15" title="15"><span class="co"># been initialized. This is useful for testing.</span></a>
<a class="sourceLine" id="cb19-16" title="16">stubs.get(<span class="st">&#39;/uni&#39;</span>) { |env| [ <span class="dv">200</span>, {}, <span class="st">&#39;urchin&#39;</span> ]}</a>
<a class="sourceLine" id="cb19-17" title="17"></a>
<a class="sourceLine" id="cb19-18" title="18">resp = test.get <span class="st">&#39;/tamago&#39;</span></a>
<a class="sourceLine" id="cb19-19" title="19">resp.body <span class="co"># =&gt; &#39;egg&#39;</span></a>
<a class="sourceLine" id="cb19-20" title="20">resp = test.get <span class="st">&#39;/ebi&#39;</span></a>
<a class="sourceLine" id="cb19-21" title="21">resp.body <span class="co"># =&gt; &#39;shrimp&#39;</span></a>
<a class="sourceLine" id="cb19-22" title="22">resp = test.get <span class="st">&#39;/uni&#39;</span></a>
<a class="sourceLine" id="cb19-23" title="23">resp.body <span class="co"># =&gt; &#39;urchin&#39;</span></a>
<a class="sourceLine" id="cb19-24" title="24">resp = test.get <span class="st">&#39;/else&#39;</span> <span class="co">#=&gt; raises &quot;no such stub&quot; error</span></a>
<a class="sourceLine" id="cb19-25" title="25"></a>
<a class="sourceLine" id="cb19-26" title="26"><span class="co"># If you like, you can treat your stubs as mocks by verifying that all of</span></a>
<a class="sourceLine" id="cb19-27" title="27"><span class="co"># the stubbed calls were made. </span><span class="al">NOTE</span><span class="co"> that this feature is still fairly</span></a>
<a class="sourceLine" id="cb19-28" title="28"><span class="co"># experimental: It will not verify the order or count of any stub, only that</span></a>
<a class="sourceLine" id="cb19-29" title="29"><span class="co"># it was called once during the course of the test.</span></a>
<a class="sourceLine" id="cb19-30" title="30">stubs.verify_stubbed_calls</a></code></pre></div>
<h2 id="supported-ruby-versions">Supported Ruby versions</h2>
<p>This library aims to support and is <a href="https://travis-ci.org/lostisland/faraday">tested against</a> the following Ruby implementations:</p>
<ul>
<li>Ruby 1.9.3+</li>
<li><a href="http://jruby.org/">JRuby</a> 1.7+</li>
<li><a href="http://rubini.us/">Rubinius</a> 2+</li>
</ul>
<p>If something doesn’t work on one of these Ruby versions, it’s a bug.</p>
<p>This library may inadvertently work (or seem to work) on other Ruby implementations, however support will only be provided for the versions listed above.</p>
<p>If you would like this library to support another Ruby version, you may volunteer to be a maintainer. Being a maintainer entails making sure all tests run and pass on that implementation. When something breaks on your implementation, you will be responsible for providing patches in a timely fashion. If critical issues for a particular implementation exist at the time of a major release, support for that Ruby version may be dropped.</p>
<h2 id="contribute">Contribute</h2>
<p>Do you want to contribute to Faraday? Open the issues page and check for the <code>help wanted</code> label! But before you start coding, please read our <a href="https://github.com/lostisland/faraday/blob/master/.github/CONTRIBUTING.md">Contributing Guide</a></p>
<h2 id="copyright">Copyright</h2>
<p>Copyright (c) 2009-2017 <a href="mailto:technoweenie@gmail.com">Rick Olson</a>, Zack Hobson. See <a href="LICENSE.md">LICENSE</a> for details.</p>
