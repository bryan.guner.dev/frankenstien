<h1 id="http-parser">HTTP Parser</h1>
<p>This is a parser for HTTP written in Java, based quite heavily on the Ryan Dahl’s C Version: <code>http-parser</code> available here:</p>
<p>http://github.com/ry/http-parser</p>
<p>It parses both requests and responses. The parser is designed to be used in performance HTTP applications.</p>
<p>Features:</p>
<ul>
<li>No dependencies (probably won’t be able to keep it up)</li>
<li>Handles persistent streams (keep-alive).</li>
<li>Decodes chunked encoding.</li>
<li>Upgrade support</li>
</ul>
<p>The parser extracts the following information from HTTP messages:</p>
<ul>
<li>Header fields and values</li>
<li>Content-Length</li>
<li>Request method</li>
<li>Response status code</li>
<li>Transfer-Encoding</li>
<li>HTTP version</li>
<li>Request URL</li>
<li>Message body</li>
</ul>
<h2 id="building">Building</h2>
<p>use <code>ant compile|test|jar</code></p>
<h2 id="usage">Usage</h2>
<p>TODO: in the present form, usage of the Java version of the parser shouldn’t be too difficult to figure out for someone familiar with the C version.</p>
<p>More documentation will follow shortly, in case you’re looking for an easy to use http library, this lib is probably not what you are looking for anyway …</p>
<p>All text after this paragraph (and most of the text above it) are from the original C version of the README and are currently only here for reference. In case you encounter any difficulties, find bugs, need help or have suggestions, feel free to contact me at (tim.becker@kuriositaet.de).</p>
<p>One <code>http_parser</code> object is used per TCP connection. Initialize the struct using <code>http_parser_init()</code> and set the callbacks. That might look something like this for a request parser:</p>
<pre><code>http_parser_settings settings;
settings.on_path = my_path_callback;
settings.on_header_field = my_header_field_callback;
/* ... */

http_parser *parser = malloc(sizeof(http_parser));
http_parser_init(parser, HTTP_REQUEST);
parser-&gt;data = my_socket;</code></pre>
<p>When data is received on the socket execute the parser and check for errors.</p>
<pre><code>size_t len = 80*1024, nparsed;
char buf[len];
ssize_t recved;

recved = recv(fd, buf, len, 0);

if (recved &lt; 0) {
  /* Handle error. */
}

/* Start up / continue the parser.
 * Note we pass recved==0 to signal that EOF has been recieved.
 */
nparsed = http_parser_execute(parser, &amp;settings, buf, recved);

if (parser-&gt;upgrade) {
  /* handle new protocol */
} else if (nparsed != recved) {
  /* Handle error. Usually just close the connection. */
}</code></pre>
<p>HTTP needs to know where the end of the stream is. For example, sometimes servers send responses without Content-Length and expect the client to consume input (for the body) until EOF. To tell http_parser about EOF, give <code>0</code> as the forth parameter to <code>http_parser_execute()</code>. Callbacks and errors can still be encountered during an EOF, so one must still be prepared to receive them.</p>
<p>Scalar valued message information such as <code>status_code</code>, <code>method</code>, and the HTTP version are stored in the parser structure. This data is only temporally stored in <code>http_parser</code> and gets reset on each new message. If this information is needed later, copy it out of the structure during the <code>headers_complete</code> callback.</p>
<p>The parser decodes the transfer-encoding for both requests and responses transparently. That is, a chunked encoding is decoded before being sent to the on_body callback.</p>
<h2 id="the-special-problem-of-upgrade">The Special Problem of Upgrade</h2>
<p>HTTP supports upgrading the connection to a different protocol. An increasingly common example of this is the Web Socket protocol which sends a request like</p>
<pre><code>    GET /demo HTTP/1.1
    Upgrade: WebSocket
    Connection: Upgrade
    Host: example.com
    Origin: http://example.com
    WebSocket-Protocol: sample</code></pre>
<p>followed by non-HTTP data.</p>
<p>(See http://tools.ietf.org/html/draft-hixie-thewebsocketprotocol-75 for more information the Web Socket protocol.)</p>
<p>To support this, the parser will treat this as a normal HTTP message without a body. Issuing both on_headers_complete and on_message_complete callbacks. However http_parser_execute() will stop parsing at the end of the headers and return.</p>
<p>The user is expected to check if <code>parser-&gt;upgrade</code> has been set to 1 after <code>http_parser_execute()</code> returns. Non-HTTP data begins at the buffer supplied offset by the return value of <code>http_parser_execute()</code>.</p>
<h2 id="callbacks">Callbacks</h2>
<p>During the <code>http_parser_execute()</code> call, the callbacks set in <code>http_parser_settings</code> will be executed. The parser maintains state and never looks behind, so buffering the data is not necessary. If you need to save certain data for later usage, you can do that from the callbacks.</p>
<p>There are two types of callbacks:</p>
<ul>
<li>notification <code>typedef int (*http_cb) (http_parser*);</code> Callbacks: on_message_begin, on_headers_complete, on_message_complete.</li>
<li>data <code>typedef int (*http_data_cb) (http_parser*, const char *at, size_t length);</code> Callbacks: (requests only) on_uri, (common) on_header_field, on_header_value, on_body;</li>
</ul>
<p>Callbacks must return 0 on success. Returning a non-zero value indicates error to the parser, making it exit immediately.</p>
<p>In case you parse HTTP message in chunks (i.e. <code>read()</code> request line from socket, parse, read half headers, parse, etc) your data callbacks may be called more than once. Http-parser guarantees that data pointer is only valid for the lifetime of callback. You can also <code>read()</code> into a heap allocated buffer to avoid copying memory around if this fits your application.</p>
<p>Reading headers may be a tricky task if you read/parse headers partially. Basically, you need to remember whether last header callback was field or value and apply following logic:</p>
<pre><code>(on_header_field and on_header_value shortened to on_h_*)
 ------------------------ ------------ --------------------------------------------
| State (prev. callback) | Callback   | Description/action                         |
 ------------------------ ------------ --------------------------------------------
| nothing (first call)   | on_h_field | Allocate new buffer and copy callback data |
|                        |            | into it                                    |
 ------------------------ ------------ --------------------------------------------
| value                  | on_h_field | New header started.                        |
|                        |            | Copy current name,value buffers to headers |
|                        |            | list and allocate new buffer for new name  |
 ------------------------ ------------ --------------------------------------------
| field                  | on_h_field | Previous name continues. Reallocate name   |
|                        |            | buffer and append callback data to it      |
 ------------------------ ------------ --------------------------------------------
| field                  | on_h_value | Value for current header started. Allocate |
|                        |            | new buffer and copy callback data to it    |
 ------------------------ ------------ --------------------------------------------
| value                  | on_h_value | Value continues. Reallocate value buffer   |
|                        |            | and append callback data to it             |
 ------------------------ ------------ --------------------------------------------</code></pre>
