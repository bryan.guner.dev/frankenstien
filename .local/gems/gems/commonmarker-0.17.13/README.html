<h1 id="commonmarker">CommonMarker</h1>
<p><a href="https://travis-ci.org/gjtorikian/commonmarker"><img src="https://travis-ci.org/gjtorikian/commonmarker.svg" alt="Build Status" /></a> <a href="http://badge.fury.io/rb/commonmarker"><img src="https://badge.fury.io/rb/commonmarker.svg" alt="Gem Version" /></a></p>
<p>Ruby wrapper for <a href="https://github.com/github/cmark">libcmark-gfm</a>, GitHub’s fork of the reference parser for CommonMark. It passes all of the C tests, and is therefore spec-complete. It also includes extensions to the CommonMark spec as documented in the <a href="http://github.github.com/gfm/">GitHub Flavored Markdown spec</a>, such as support for tables, strikethroughs, and autolinking.</p>
<p>For more information on available extensions, see <a href="#extensions">the documentation below</a>.</p>
<h2 id="installation">Installation</h2>
<p>Add this line to your application’s Gemfile:</p>
<pre><code>gem &#39;commonmarker&#39;</code></pre>
<p>And then execute:</p>
<pre><code>$ bundle</code></pre>
<p>Or install it yourself as:</p>
<pre><code>$ gem install commonmarker</code></pre>
<h2 id="usage">Usage</h2>
<h3 id="converting-to-html">Converting to HTML</h3>
<p>Call <code>render_html</code> on a string to convert it to HTML:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb4-1" title="1">require <span class="st">&#39;commonmarker&#39;</span></a>
<a class="sourceLine" id="cb4-2" title="2"><span class="dt">CommonMarker</span>.render_html(<span class="st">&#39;Hi *there*&#39;</span>, <span class="st">:DEFAULT</span>)</a>
<a class="sourceLine" id="cb4-3" title="3"><span class="co"># &lt;p&gt;Hi &lt;em&gt;there&lt;/em&gt;&lt;/p&gt;\n</span></a></code></pre></div>
<p>The second argument is optional–<a href="#options">see below</a> for more information.</p>
<h3 id="generating-a-document">Generating a document</h3>
<p>You can also parse a string to receive a <code>Document</code> node. You can then print that node to HTML, iterate over the children, and other fun node stuff. For example:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb5-1" title="1">require <span class="st">&#39;commonmarker&#39;</span></a>
<a class="sourceLine" id="cb5-2" title="2"></a>
<a class="sourceLine" id="cb5-3" title="3">doc = <span class="dt">CommonMarker</span>.render_doc(<span class="st">&#39;*Hello* world&#39;</span>, <span class="st">:DEFAULT</span>)</a>
<a class="sourceLine" id="cb5-4" title="4">puts(doc.to_html) <span class="co"># &lt;p&gt;Hi &lt;em&gt;there&lt;/em&gt;&lt;/p&gt;\n</span></a>
<a class="sourceLine" id="cb5-5" title="5"></a>
<a class="sourceLine" id="cb5-6" title="6">doc.walk <span class="kw">do</span> |node|</a>
<a class="sourceLine" id="cb5-7" title="7">  puts node.type <span class="co"># [:document, :paragraph, :text, :emph, :text]</span></a>
<a class="sourceLine" id="cb5-8" title="8"><span class="kw">end</span></a></code></pre></div>
<p>The second argument is optional–<a href="#options">see below</a> for more information.</p>
<h4 id="example-walking-the-ast">Example: walking the AST</h4>
<p>You can use <code>walk</code> or <code>each</code> to iterate over nodes:</p>
<ul>
<li><code>walk</code> will iterate on a node and recursively iterate on a node’s children.</li>
<li><code>each</code> will iterate on a node and its children, but no further.</li>
</ul>
<div class="sourceCode" id="cb6"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb6-1" title="1">require <span class="st">&#39;commonmarker&#39;</span></a>
<a class="sourceLine" id="cb6-2" title="2"></a>
<a class="sourceLine" id="cb6-3" title="3"><span class="co"># parse the files specified on the command line</span></a>
<a class="sourceLine" id="cb6-4" title="4">doc = <span class="dt">CommonMarker</span>.render_doc(<span class="st">&quot;# The site\n\n [GitHub](https://www.github.com)&quot;</span>)</a>
<a class="sourceLine" id="cb6-5" title="5"></a>
<a class="sourceLine" id="cb6-6" title="6"><span class="co"># Walk tree and print out URLs for links</span></a>
<a class="sourceLine" id="cb6-7" title="7">doc.walk <span class="kw">do</span> |node|</a>
<a class="sourceLine" id="cb6-8" title="8">  <span class="kw">if</span> node.type == <span class="st">:link</span></a>
<a class="sourceLine" id="cb6-9" title="9">    printf(<span class="st">&quot;URL = %s\n&quot;</span>, node.url)</a>
<a class="sourceLine" id="cb6-10" title="10">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb6-11" title="11"><span class="kw">end</span></a>
<a class="sourceLine" id="cb6-12" title="12"></a>
<a class="sourceLine" id="cb6-13" title="13"><span class="co"># Capitalize all regular text in headers</span></a>
<a class="sourceLine" id="cb6-14" title="14">doc.walk <span class="kw">do</span> |node|</a>
<a class="sourceLine" id="cb6-15" title="15">  <span class="kw">if</span> node.type == <span class="st">:header</span></a>
<a class="sourceLine" id="cb6-16" title="16">    node.each <span class="kw">do</span> |subnode|</a>
<a class="sourceLine" id="cb6-17" title="17">      <span class="kw">if</span> subnode.type == <span class="st">:text</span></a>
<a class="sourceLine" id="cb6-18" title="18">        subnode.string_content = subnode.string_content.upcase</a>
<a class="sourceLine" id="cb6-19" title="19">      <span class="kw">end</span></a>
<a class="sourceLine" id="cb6-20" title="20">    <span class="kw">end</span></a>
<a class="sourceLine" id="cb6-21" title="21">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb6-22" title="22"><span class="kw">end</span></a>
<a class="sourceLine" id="cb6-23" title="23"></a>
<a class="sourceLine" id="cb6-24" title="24"><span class="co"># Transform links to regular text</span></a>
<a class="sourceLine" id="cb6-25" title="25">doc.walk <span class="kw">do</span> |node|</a>
<a class="sourceLine" id="cb6-26" title="26">  <span class="kw">if</span> node.type == <span class="st">:link</span></a>
<a class="sourceLine" id="cb6-27" title="27">    node.insert_before(node.first_child)</a>
<a class="sourceLine" id="cb6-28" title="28">    node.delete</a>
<a class="sourceLine" id="cb6-29" title="29">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb6-30" title="30"><span class="kw">end</span></a></code></pre></div>
<h3 id="creating-a-custom-renderer">Creating a custom renderer</h3>
<p>You can also derive a class from CommonMarker’s <code>HtmlRenderer</code> class. This produces slower output, but is far more customizable. For example:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb7-1" title="1"><span class="kw">class</span> <span class="dt">MyHtmlRenderer</span> &lt; <span class="dt">CommonMarker</span>::<span class="dt">HtmlRenderer</span></a>
<a class="sourceLine" id="cb7-2" title="2">  <span class="kw">def</span> initialize</a>
<a class="sourceLine" id="cb7-3" title="3">    <span class="dv">super</span></a>
<a class="sourceLine" id="cb7-4" title="4">    <span class="ot">@headerid</span> = <span class="dv">1</span></a>
<a class="sourceLine" id="cb7-5" title="5">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb7-6" title="6"></a>
<a class="sourceLine" id="cb7-7" title="7">  <span class="kw">def</span> header(node)</a>
<a class="sourceLine" id="cb7-8" title="8">    block <span class="kw">do</span></a>
<a class="sourceLine" id="cb7-9" title="9">      out(<span class="st">&quot;&lt;h&quot;</span>, node.header_level, <span class="st">&quot; id=\&quot;&quot;</span>, <span class="ot">@headerid</span>, <span class="st">&quot;\&quot;&gt;&quot;</span>,</a>
<a class="sourceLine" id="cb7-10" title="10">               <span class="st">:children</span>, <span class="st">&quot;&lt;/h&quot;</span>, node.header_level, <span class="st">&quot;&gt;&quot;</span>)</a>
<a class="sourceLine" id="cb7-11" title="11">      <span class="ot">@headerid</span> += <span class="dv">1</span></a>
<a class="sourceLine" id="cb7-12" title="12">    <span class="kw">end</span></a>
<a class="sourceLine" id="cb7-13" title="13">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb7-14" title="14"><span class="kw">end</span></a>
<a class="sourceLine" id="cb7-15" title="15"></a>
<a class="sourceLine" id="cb7-16" title="16"><span class="co"># this renderer prints directly to STDOUT, instead</span></a>
<a class="sourceLine" id="cb7-17" title="17"><span class="co"># of returning a string</span></a>
<a class="sourceLine" id="cb7-18" title="18">myrenderer = <span class="dt">MyHtmlRenderer</span>.new</a>
<a class="sourceLine" id="cb7-19" title="19">print(myrenderer.render(doc))</a>
<a class="sourceLine" id="cb7-20" title="20"></a>
<a class="sourceLine" id="cb7-21" title="21"><span class="co"># Print any warnings to STDERR</span></a>
<a class="sourceLine" id="cb7-22" title="22">renderer.warnings.each <span class="kw">do</span> |w|</a>
<a class="sourceLine" id="cb7-23" title="23">  <span class="dt">STDERR</span>.write(<span class="st">&quot;</span><span class="ot">#{</span>w<span class="ot">}</span><span class="st">\n&quot;</span>)</a>
<a class="sourceLine" id="cb7-24" title="24"><span class="kw">end</span></a></code></pre></div>
<h2 id="options">Options</h2>
<p>CommonMarker accepts the same options that CMark does, as symbols. Note that there is a distinction in CMark for “parse” options and “render” options, which are represented in the tables below.</p>
<h3 id="parse-options">Parse options</h3>
<table>
<colgroup>
<col style="width: 72%" />
<col style="width: 27%" />
</colgroup>
<thead>
<tr class="header">
<th>Name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>:DEFAULT</code></td>
<td>The default parsing system.</td>
</tr>
<tr class="even">
<td><code>:FOOTNOTES</code></td>
<td>Parse footnotes.</td>
</tr>
<tr class="odd">
<td><code>:LIBERAL_HTML_TAG</code></td>
<td>Support liberal parsing of inline HTML tags.</td>
</tr>
<tr class="even">
<td><code>:SMART</code></td>
<td>Use smart punctuation (curly quotes, etc.).</td>
</tr>
<tr class="odd">
<td><code>:STRIKETHROUGH_DOUBLE_TILDE</code></td>
<td>Parse strikethroughs by double tildes (compatibility with <a href="https://github.com/vmg/redcarpet">redcarpet</a>)</td>
</tr>
<tr class="even">
<td><code>:VALIDATE_UTF8</code></td>
<td>Replace illegal sequences with the replacement character <code>U+FFFD</code>.</td>
</tr>
</tbody>
</table>
<h3 id="render-options">Render options</h3>
<table>
<colgroup>
<col style="width: 62%" />
<col style="width: 37%" />
</colgroup>
<thead>
<tr class="header">
<th>Name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>:DEFAULT</code></td>
<td>The default rendering system.</td>
</tr>
<tr class="even">
<td><code>:GITHUB_PRE_LANG</code></td>
<td>Use GitHub-style <code>&lt;pre lang&gt;</code> for fenced code blocks.</td>
</tr>
<tr class="odd">
<td><code>:HARDBREAKS</code></td>
<td>Treat <code>\n</code> as hardbreaks (by adding <code>&lt;br/&gt;</code>).</td>
</tr>
<tr class="even">
<td><code>:NOBREAKS</code></td>
<td>Translate <code>\n</code> in the source to a single whitespace.</td>
</tr>
<tr class="odd">
<td><code>:SAFE</code></td>
<td>Suppress raw HTML and unsafe links.</td>
</tr>
<tr class="even">
<td><code>:SOURCEPOS</code></td>
<td>Include source position in rendered HTML.</td>
</tr>
<tr class="odd">
<td><code>:TABLE_PREFER_STYLE_ATTRIBUTES</code></td>
<td>Use <code>style</code> insted of <code>align</code> for table cells</td>
</tr>
<tr class="even">
<td><code>:FULL_INFO_STRING</code></td>
<td>Include full info strings of code blocks in separate attribute</td>
</tr>
</tbody>
</table>
<h3 id="passing-options">Passing options</h3>
<p>To apply a single option, pass it in as a symbol argument:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb8-1" title="1"><span class="dt">CommonMarker</span>.render_doc(<span class="st">&quot;\&quot;Hello,\&quot; said the spider.&quot;</span>, <span class="st">:SMART</span>)</a>
<a class="sourceLine" id="cb8-2" title="2"><span class="co"># &lt;p&gt;“Hello,” said the spider.&lt;/p&gt;\n</span></a></code></pre></div>
<p>To have multiple options applied, pass in an array of symbols:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb9-1" title="1"><span class="dt">CommonMarker</span>.render_html(<span class="st">&quot;\&quot;&#39;Shelob&#39; is my name.\&quot;&quot;</span>, [<span class="st">:HARDBREAKS</span>, <span class="st">:SOURCEPOS</span>])</a></code></pre></div>
<p>For more information on these options, see <a href="https://git.io/v7nh1">the CMark documentation</a>.</p>
<h2 id="extensions">Extensions</h2>
<p>Both <code>render_html</code> and <code>render_doc</code> take an optional third argument defining the extensions you want enabled as your CommonMark document is being processed. The documentation for these extensions are <a href="https://github.github.com/gfm/">defined in this spec</a>, and the rationale is provided <a href="https://githubengineering.com/a-formal-spec-for-github-markdown/">in this blog post</a>.</p>
<p>The available extensions are:</p>
<ul>
<li><code>:table</code> - This provides support for tables.</li>
<li><code>:strikethrough</code> - This provides support for strikethroughs.</li>
<li><code>:autolink</code> - This provides support for automatically converting URLs to anchor tags.</li>
<li><code>:tagfilter</code> - This strips out <a href="https://github.github.com/gfm/#disallowed-raw-html-extension-">several “unsafe” HTML tags</a> from being used.</li>
</ul>
<h2 id="developing-locally">Developing locally</h2>
<p>After cloning the repo:</p>
<pre><code>script/bootstrap
bundle exec rake compile</code></pre>
<p>If there were no errors, you’re done! Otherwise, make sure to follow the CMark dependency instructions.</p>
<h2 id="benchmarks">Benchmarks</h2>
<p>Some rough benchmarks:</p>
<pre><code>$ bundle exec rake benchmark

input size = 11063727 bytes

redcarpet
  0.070000   0.020000   0.090000 (  0.079641)
github-markdown
  0.070000   0.010000   0.080000 (  0.083535)
commonmarker with to_html
  0.100000   0.010000   0.110000 (  0.111947)
commonmarker with ruby HtmlRenderer
  1.830000   0.030000   1.860000 (  1.866203)
kramdown
  4.610000   0.070000   4.680000 (  4.678398)</code></pre>
