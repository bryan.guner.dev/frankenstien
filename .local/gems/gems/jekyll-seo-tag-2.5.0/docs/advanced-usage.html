<h2 id="advanced-usage">Advanced usage</h2>
<p>Jekyll SEO Tag is designed to implement SEO best practices by default and to be the right fit for most sites right out of the box. If for some reason, you need more control over the output, read on:</p>
<h3 id="disabling-title-output">Disabling <code>&lt;title&gt;</code> output</h3>
<p>If for some reason, you don’t want the plugin to output <code>&lt;title&gt;</code> tags on each page, simply invoke the plugin within your template like so:</p>
<!-- {% raw %} -->
<pre><code>{% seo title=false %}</code></pre>
<!-- {% endraw %} -->
<h3 id="author-information">Author information</h3>
<p>Author information is used to propagate the <code>creator</code> field of Twitter summary cards. This should be an author-specific, not site-wide Twitter handle (the site-wide username be stored as <code>site.twitter.username</code>).</p>
<p><em>TL;DR: In most cases, put <code>author: [your Twitter handle]</code> in the document’s front matter, for sites with multiple authors. If you need something more complicated, read on.</em></p>
<p>There are several ways to convey this author-specific information. Author information is found in the following order of priority:</p>
<ol type="1">
<li>An <code>author</code> object, in the documents’s front matter, e.g.:</li>
</ol>
<div class="sourceCode" id="cb2"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb2-1" title="1"><span class="fu">author:</span></a>
<a class="sourceLine" id="cb2-2" title="2">  <span class="fu">twitter:</span><span class="at"> benbalter</span></a></code></pre></div>
<ol start="2" type="1">
<li>An <code>author</code> object, in the site’s <code>_config.yml</code>, e.g.:</li>
</ol>
<div class="sourceCode" id="cb3"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb3-1" title="1"><span class="fu">author:</span></a>
<a class="sourceLine" id="cb3-2" title="2">  <span class="fu">twitter:</span><span class="at"> benbalter</span></a></code></pre></div>
<ol start="3" type="1">
<li><code>site.data.authors[author]</code>, if an author is specified in the document’s front matter, and a corresponding key exists in <code>site.data.authors</code>. E.g., you have the following in the document’s front matter:</li>
</ol>
<div class="sourceCode" id="cb4"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb4-1" title="1"><span class="fu">author:</span><span class="at"> benbalter</span></a></code></pre></div>
<p>And you have the following in <code>_data/authors.yml</code>:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb5-1" title="1"><span class="fu">benbalter:</span></a>
<a class="sourceLine" id="cb5-2" title="2">  <span class="fu">picture:</span><span class="at"> /img/benbalter.png</span></a>
<a class="sourceLine" id="cb5-3" title="3">  <span class="fu">twitter:</span><span class="at"> jekyllrb</span></a>
<a class="sourceLine" id="cb5-4" title="4"></a>
<a class="sourceLine" id="cb5-5" title="5"><span class="fu">potus:</span></a>
<a class="sourceLine" id="cb5-6" title="6">  <span class="fu">picture:</span><span class="at"> /img/potus.png</span></a>
<a class="sourceLine" id="cb5-7" title="7">  <span class="fu">twitter:</span><span class="at"> whitehouse</span></a></code></pre></div>
<p>In the above example, the author <code>benbalter</code>’s Twitter handle will be resolved to <code>@jekyllrb</code>. This allows you to centralize author information in a single <code>_data/authors</code> file for site with many authors that require more than just the author’s username.</p>
<p><em>Pro-tip: If <code>authors</code> is present in the document’s front matter as an array (and <code>author</code> is not), the plugin will use the first author listed, as Twitter supports only one author.</em></p>
<ol start="4" type="1">
<li>An author in the document’s front matter (the simplest way), e.g.:</li>
</ol>
<div class="sourceCode" id="cb6"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb6-1" title="1"><span class="fu">author:</span><span class="at"> benbalter</span></a></code></pre></div>
<ol start="5" type="1">
<li>An author in the site’s <code>_config.yml</code>, e.g.:</li>
</ol>
<div class="sourceCode" id="cb7"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb7-1" title="1"><span class="fu">author:</span><span class="at"> benbalter</span></a></code></pre></div>
<h3 id="customizing-json-ld-output">Customizing JSON-LD output</h3>
<p>The following options can be set for any particular page. While the default options are meant to serve most users in the most common circumstances, there may be situations where more precise control is necessary.</p>
<ul>
<li><code>seo</code>
<ul>
<li><code>name</code> - If the name of the thing that the page represents is different from the page title. (i.e.: “Frank’s Café” vs “Welcome to Frank’s Café”)</li>
<li><code>type</code> - The type of things that the page represents. This must be a <a href="http://schema.org/docs/schemas.html">Schema.org type</a>, and will probably usually be something like <a href="http://schema.org/BlogPosting"><code>BlogPosting</code></a>, <a href="http://schema.org/NewsArticle"><code>NewsArticle</code></a>, <a href="http://schema.org/Person"><code>Person</code></a>, <a href="http://schema.org/Organization"><code>Organization</code></a>, etc.</li>
<li><code>links</code> - An array of other URLs that represent the same thing that this page represents. For instance, Jane’s bio page might include links to Jane’s GitHub and Twitter profiles.</li>
</ul></li>
</ul>
<h3 id="customizing-image-output">Customizing image output</h3>
<p>For most users, setting <code>image: [path-to-image]</code> on a per-page basis should be enough. If you need more control over how images are represented, the <code>image</code> property can also be an object, with the following options:</p>
<ul>
<li><code>path</code> - The relative path to the image. Same as <code>image: [path-to-image]</code></li>
<li><code>height</code> - The height of the Open Graph (<code>og:image</code>) image</li>
<li><code>width</code> - The width of the Open Graph (<code>og:image</code>) image</li>
</ul>
<p>You can use any of the above, optional properties, like so:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb8-1" title="1"><span class="fu">image:</span></a>
<a class="sourceLine" id="cb8-2" title="2">  <span class="fu">path:</span><span class="at"> /img/twitter.png</span></a>
<a class="sourceLine" id="cb8-3" title="3">  <span class="fu">height:</span><span class="at"> </span><span class="dv">100</span></a>
<a class="sourceLine" id="cb8-4" title="4">  <span class="fu">width:</span><span class="at"> </span><span class="dv">100</span></a></code></pre></div>
<h3 id="setting-a-default-image">Setting a default image</h3>
<p>You can define a default image using <a href="https://jekyllrb.com/docs/configuration/#front-matter-defaults">Front Matter default</a>, to provide a default Twitter Card or OGP image to all of your posts and pages.</p>
<p>Here is a very basic example, that you are encouraged to adapt to your needs:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb9-1" title="1"><span class="fu">defaults:</span></a>
<a class="sourceLine" id="cb9-2" title="2">  <span class="kw">-</span> <span class="fu">scope:</span></a>
<a class="sourceLine" id="cb9-3" title="3">      <span class="fu">path:</span><span class="at"> </span><span class="st">&quot;&quot;</span></a>
<a class="sourceLine" id="cb9-4" title="4">    <span class="fu">values:</span></a>
<a class="sourceLine" id="cb9-5" title="5">      <span class="fu">image:</span><span class="at"> /assets/images/default-card.png</span></a></code></pre></div>
<h3 id="smartypants-titles">SmartyPants Titles</h3>
<p>Titles will be processed using <a href="https://jekyllrb.com/docs/templates/">Jekyll’s <code>smartify</code> filter</a>. This will use SmartyPants to translate plain ASCII punctuation into “smart” typographic punctuation. This will not render or strip any Markdown you may be using in a page title.</p>
<h3 id="setting-customized-canonical-url">Setting customized Canonical URL</h3>
<p>You can set custom Canonical URL for a page by specifying canonical_url option in page front-matter. E.g., you have the following in the page’s front matter:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb10-1" title="1"><span class="fu">layout:</span><span class="at"> post</span></a>
<a class="sourceLine" id="cb10-2" title="2"><span class="fu">title:</span><span class="at"> Title of Your Post</span></a>
<a class="sourceLine" id="cb10-3" title="3"><span class="fu">canonical_url:</span><span class="at"> </span><span class="st">&#39;https://github.com/jekyll/jekyll-seo-tag/&#39;</span></a></code></pre></div>
<p>Which will generate canonical_url with specified link in canonical_url.</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode html"><code class="sourceCode html"><a class="sourceLine" id="cb11-1" title="1"><span class="kw">&lt;link</span><span class="ot"> rel=</span><span class="st">&quot;canonical&quot;</span><span class="ot"> href=</span><span class="st">&quot;https://github.com/jekyll/jekyll-seo-tag/&quot;</span> <span class="kw">/&gt;</span></a></code></pre></div>
<p>If no canonical_url option was specified, then uses page url for generating canonical_url. E.g., you have not specified canonical_url in front-matter:</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb12-1" title="1"><span class="fu">layout:</span><span class="at"> post</span></a>
<a class="sourceLine" id="cb12-2" title="2"><span class="fu">title:</span><span class="at"> Title of Your Post</span></a></code></pre></div>
<p>Which will generate following canonical_url:</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode html"><code class="sourceCode html"><a class="sourceLine" id="cb13-1" title="1"><span class="kw">&lt;link</span><span class="ot"> rel=</span><span class="st">&quot;canonical&quot;</span><span class="ot"> href=</span><span class="st">&quot;http://yoursite.com/title-of-your-post&quot;</span> <span class="kw">/&gt;</span></a></code></pre></div>
